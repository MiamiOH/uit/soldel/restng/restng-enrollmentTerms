<?php

namespace MiamiOH\RestngEnrollmentTerms\Services;

class EnrollmentTerms extends BannerBase
{

    function queryEnrollmentTerms($userid, $termid = '')
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();
        $role = isset($options['role']) ? $options['role'] : 'all';
        $myuserid = $this->getApiUser()->getUsername();
        $pidm = $this->getPidm($myuserid);

        $aok = $this->accessOK($myuserid, $pidm, $userid, $termid);
        if (!$aok)
            return $this->unauthorizedRequest();

        $spec = isset($options['specialUser']) ? $options['specialUser'] : 'N';
        if ($spec == 'Y')
            $pidm = $this->getPidm($userid);

        $sql = '';

        if ($role == 'all' || $role == 'student') {
            $sql =
                'SELECT DISTINCT szbuniq_unique_id, sfrstcr_term_code
FROM sirasgn, sfrstcr, szbuniq
WHERE sirasgn_crn = sfrstcr_crn
AND sirasgn_term_code = sfrstcr_term_code
AND sfrstcr_pidm = szbuniq_pidm';

            if (strcmp($userid, $myuserid)) {
                $sql .= ' AND sfrstcr_pidm = :PIDM';
                $vars['PIDM'] = $pidm;
            }
            if ($userid) {
                $vars['STUDENT'] = $userid;
                $sql .= ' AND szbuniq_unique_id = :STUDENT';
            }
            if ($termid) {
                $vars['TERM_ID'] = $termid;
                $sql .= ' AND sfrstcr_term_code = :TERM_ID';
            }

        }

        if ($userid && ($role == 'all' || $role == 'instructor')) {
            // A student may not request an instructor's data
            if ($sql)
                $sql .= ' UNION ALL ';
            $sql .=
                'SELECT DISTINCT szbuniq_unique_id, sirasgn_term_code
FROM sirasgn, ssrmeet, sfrstcr, szbuniq
WHERE sirasgn_crn = ssrmeet_crn
AND sirasgn_term_code = ssrmeet_term_code
AND sirasgn_category = ssrmeet_catagory
AND sirasgn_crn = sfrstcr_crn
AND sirasgn_term_code = sfrstcr_term_code
AND sfrstcr_pidm = szbuniq_pidm
AND sirasgn_pidm = :PIDM';

            $vars['PIDM'] = $pidm;
            if ($termid) {
                $vars['TERM_ID'] = $termid;
                $sql .= ' AND sirasgn_term_code = :TERM_ID';
            }
            if ($userid) {
                $vars['STUDENT'] = $userid;
                $sql .= ' AND szbuniq_unique_id = :STUDENT';
            }
        }

        $data = array();
        $sth = $this->bindquery($sql, $vars);
        while ($row = $sth->fetchrow_assoc()) {
//			$newrow = $row;
            $newrow['academicTermResource'] = $this->academicTermResource($row['sfrstcr_term_code']);
            $newrow['enrollmentResource'] = $this->enrollmentResource($row['sfrstcr_term_code'], $row['szbuniq_unique_id'], $row['szbuniq_unique_id']);
            $newrow['enrollmentTermResource'] = $this->enrollmentTermResource($row['sfrstcr_term_code'], $row['szbuniq_unique_id']);

            $data[] = $newrow;
        }
        if ($termid)
            return $this->makeSingleResponse($data[0]);
        else
            return $this->makeCollectionResponse($data);
    }

    function getEnrollmentTerms()
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $userid = isset($options['username']) ? $options['username'] : '';
        return $this->queryEnrollmentTerms($userid);
    }

    function getUserEnrollmentsTerms()
    {
        $request = $this->getRequest();
        $username = $request->getResourceParam('username');
        return $this->queryEnrollmentTerms($username);
    }

    function getUserEnrollmentsTermID()
    {
        $request = $this->getRequest();
        $username = $request->getResourceParam('username');
        $termid = $request->getResourceParam('termId');
        return $this->queryEnrollmentTerms($username, $termid);
    }

}
