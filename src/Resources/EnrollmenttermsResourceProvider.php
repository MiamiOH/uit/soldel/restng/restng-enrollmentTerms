<?php

namespace MiamiOH\RestngEnrollmentTerms\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EnrollmenttermsResourceProvider extends ResourceProvider
{

    private $special_user = array(
        'enum' => ['Y', 'N'],
        'description' => 'special user indicator (Y or N)'
    );
    private $role_option = array(
        'enum' => ['student', 'instructor', 'all'],
        'description' => 'student, instructor, or all'
    );

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'academic.enrollmentTerm',
            'type' => 'object',
            'properties' => array(
                'enrollmentTermResource' => array('type' => 'resource'),
                'academicResource' => array('type' => 'resource'),
                'enrollmentResource' => array('type' => 'resource')
            )
        ));

        $this->addDefinition(array(
            'name' => 'academic.enrollmentTerm.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/academic.enrollmentTerm'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'BannerREST.Test.enrollmentTermsSvc',
            'class' => 'MiamiOH\RestngEnrollmentTerms\Services\EnrollmentTerms',
            'description' => 'Enrollments terms',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'academic.banner.test.v2.enrollmentTerms',
            'description' => 'List enrollment terms.',
            'pattern' => '/academic/banner/v2/enrollmentTerms',
            'service' => 'BannerREST.Test.enrollmentTermsSvc',
            'tags' => array('enrollmentTerms'),
            'method' => 'getEnrollmentTerms',
            'returnType' => 'collection',
            'options' => array(
                'application' => array('description' => 'Application'),
                'username' => array('description' => 'username'),
                'role' => $this->role_option,
                'specialUser' => $this->special_user,
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'An enrollment',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/academic.enrollmentTerm.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'academic.banner.test.v2.userEnrollmentTerms',
            'description' => 'List terms.',
            'pattern' => '/academic/banner/v2/enrollmentTerms/:username',
            'service' => 'BannerREST.Test.enrollmentTermsSvc',
            'tags' => array('enrollmentTerms'),
            'method' => 'getUserEnrollmentsTerms',
            'returnType' => 'collection',
            'params' => array(
                'username' => array('description' => 'username'),
            ),
            'options' => array(
                'application' => array('description' => 'Application'),
                'role' => $this->role_option,
                'specialUser' => $this->special_user,
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'An enrollment',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/academic.enrollmentTerm.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'academic.banner.test.v2.userEnrollmentTermsByTerm',
            'description' => 'List terms.',
            'pattern' => '/academic/banner/v2/enrollmentTerms/:username/:termId',
            'service' => 'BannerREST.Test.enrollmentTermsSvc',
            'tags' => array('enrollmentTerms'),
            'method' => 'getUserEnrollmentsTermID',
            'returnType' => 'collection',
            'params' => array(
                'username' => array('description' => 'username'),
                'termId' => array('description' => 'unique id of the term'),
            ),
            'options' => array(
                'application' => array('description' => 'Application'),
                'role' => $this->role_option,
                'specialUser' => $this->special_user,
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'An enrollment',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/academic.enrollmentTerm.Collection',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}