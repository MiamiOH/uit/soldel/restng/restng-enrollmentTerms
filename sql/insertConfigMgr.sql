set define off;

delete from cm_config where config_application = 'BannerREST';

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('BannerREST', 'courseSectionV2_URL_Template',
        'text', 'scalar', 'REST',
        'Template URL for constructing course section resources',
        'http://wsdev.miamioh.edu/courseSectionV2/{term}/{crn}',
        sysdate, 'tepeds');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('BannerREST', 'person_URL_Template',
        'text', 'scalar', 'REST',
        'Template URL for constructing person resources',
        'http://wsdev.miamioh.edu/person/{uniqueid}',
        sysdate, 'tepeds');

