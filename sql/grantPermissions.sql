grant select on scbcrse to muws_gen_rl;
grant select on sfrstcr to muws_gen_rl;
grant select on Sirasgn to muws_gen_rl;
grant select on Ssbsect to muws_gen_rl;
grant select on Ssrmeet to muws_gen_rl;
grant select on stvbldg to muws_gen_rl;
grant select on stvrsts to muws_gen_rl;
grant select on Stvterm to muws_gen_rl;
grant select on szbuniq to muws_gen_rl;
grant select on Szvgrde to muws_gen_rl;
grant update on sfrstcr to muws_sec_rl;

grant select on szvgrde to muws_gen_rl;
grant select on sobptrm to muws_gen_rl;
grant select on sobterm to muws_gen_rl;
grant select on szvgmvt to muws_gen_rl;

grant execute on fz_midterm_grde_reqd_ind to muws_gen_rl;
